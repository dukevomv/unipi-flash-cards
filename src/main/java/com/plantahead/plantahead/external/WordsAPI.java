package com.plantahead.plantahead.external;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;


public class WordsAPI {

    private String apiKey = "cf00d62205msha33e496d9adf34bp1aef8fjsn45ff585fe062";
    private String apiHost = "wordsapiv1.p.rapidapi.com";

    public JsonNode getRandomWord() throws JsonProcessingException {
        final String uri = "https://wordsapiv1.p.rapidapi.com/words/?random=true";
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-rapidapi-key", this.apiKey);
        headers.set("x-rapidapi-host", this.apiHost);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        return root;
    }
}
