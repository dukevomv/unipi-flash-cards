package com.plantahead.plantahead.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "games")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column(name = "username")
    private String username;
    @Column(name = "score")
    private int score;
    @Column(name = "lives")
    private int lives;
    @Column(name = "started_at")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date started_at;
    @Column(name = "finished_at")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date finished_at;

}