package com.plantahead.plantahead.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.plantahead.plantahead.external.WordsAPI;
import com.plantahead.plantahead.helper.*;
import com.plantahead.plantahead.model.Game;
import com.plantahead.plantahead.model.User;
import com.plantahead.plantahead.model.Word;
import com.plantahead.plantahead.repository.WordRepository;
import com.plantahead.plantahead.repository.GameRepository;
import com.plantahead.plantahead.service.GameService;
import com.plantahead.plantahead.service.UserService;
import com.plantahead.plantahead.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ApiController {
    @Autowired
    private UserService userService;
    @Autowired
    private GameService gameService;
    @Autowired
    private WordService wordService;
    @Autowired
    private WordRepository wordRepository;
    @Autowired
    private GameRepository gameRepository;

    @GetMapping("/api/words")
    public ResponseEntity<?> getWords(@Valid @RequestBody(required = false) WordResponse data, Errors errors) throws JsonProcessingException {
        WordResponse result = new WordResponse();
        List<Word> someWords = wordService.getSome();
        Collections.shuffle(someWords);
        result.setResult(someWords);
        result.setMsg("success");
        return ResponseEntity.ok(result);
    }

    @PostMapping("/api/start-game")
    public ResponseEntity<?> startGame(@Valid @RequestBody StartGameRequest body, Errors errors) throws JsonProcessingException {
        GameResponse result = new GameResponse();
        Game game = new Game();
        game.setUsername(body.getUsername());
        game.setLives(3);
        gameRepository.save(game);
        result.setId(game.getId());
        result.setMsg("gameCreated");
        return ResponseEntity.ok(result);
    }

    @PostMapping("/api/vote")
    public ResponseEntity<?> vote(@Valid @RequestBody VoteRequest body, Errors errors) throws JsonProcessingException, ChangeSetPersister.NotFoundException {
        VoteResponse result = new VoteResponse();
        Word word = wordService.getByPair(body.getMain(),body.getSecondary());
        Game game = gameService.findById(body.getGame_id());
        Integer correctPoints = 150;
        if(game != null && game.getLives() > 0){
            if(word.getIs_synonym() == body.getIs_synonym()){
                result.setCorrect(true);
                result.setPoints(correctPoints);
                game.setScore(game.getScore() + correctPoints);
            } else {
                result.setCorrect(false);
                result.setLife(-1);
                game.setLives(game.getLives() - 1);
                if(game.getLives() == 0){
                    result.setCanContinue(false);
                    result.setFinalScore(game.getScore());
                    User user = userService.findUserByUsername(game.getUsername());
                    if(user.getPoints() < game.getScore()){
                        result.setIsHighscore(true);
                        user.setPoints(game.getScore());
                        userService.saveUser(user);
                    }
                }
            }
            gameRepository.save(game);
        } else {
            result.setCanContinue(false);
        }
        return ResponseEntity.ok(result);
    }
}