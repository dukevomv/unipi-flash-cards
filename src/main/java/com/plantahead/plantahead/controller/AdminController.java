package com.plantahead.plantahead.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.plantahead.plantahead.external.WordsAPI;
import com.plantahead.plantahead.helper.WordResponse;
import com.plantahead.plantahead.model.User;
import com.plantahead.plantahead.model.Word;
import com.plantahead.plantahead.service.UserService;
import com.plantahead.plantahead.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
public class AdminController {
    @Autowired
    private UserService userService;
    @Autowired
    private WordService wordService;

    @RequestMapping(value="/admin/words", method = RequestMethod.GET)
    public ModelAndView words(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("words",wordService.getSome());
        modelAndView.setViewName("admin/words");
        return modelAndView;
    }

    @GetMapping("/admin/words/{pageNo}")
    public ModelAndView findPaginated(@PathVariable(value = "pageNo") int pageNo,
                                @RequestParam(defaultValue = "main",name="sortField") String sortField,
                                @RequestParam(defaultValue = "asc",name="sortDir") String sortDir) {

        int pageSize = 10;
        Page< Word > page = wordService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List< Word > listWords = page.getContent();

        int from = 1;
        int to = 10;
        if(pageNo > 5){
            from = pageNo - 5;
            to = from + 10;
            if(to > page.getTotalPages()){
                to = page.getTotalPages();
                from = to - 10;
            }
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("currentPage", pageNo);
        modelAndView.addObject("fromPage", from);
        modelAndView.addObject("toPage", to);
        modelAndView.addObject("totalPages", page.getTotalPages());
        modelAndView.addObject("totalItems", page.getTotalElements());

        modelAndView.addObject("sortField", sortField);
        modelAndView.addObject("sortDir", sortDir);
        modelAndView.addObject("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        modelAndView.addObject("words", listWords);
        modelAndView.setViewName("admin/words");
        return modelAndView;
    }

    @RequestMapping(value="/admin/fetch-word", method = RequestMethod.POST)
    public ModelAndView getWords() throws JsonProcessingException {
        WordsAPI api = new WordsAPI();
        JsonNode randomWord = api.getRandomWord();
        ModelAndView modelAndView = new ModelAndView();

        String word = randomWord.path("word").asText();
        int maxAmount = 10;
        String[] synonyms = new String[maxAmount];
        String[] antonyms = new String[maxAmount];

        JsonNode results = randomWord.path("results");
        if(!results.isEmpty() && results.isArray()){
            int synIndex = 0;
            int antIndex = 0;
            for (final JsonNode item : results) {
                final JsonNode syn = item.path("synonyms");
                if (!syn.isEmpty() && syn.isArray()) {
                    for (final JsonNode objNode : syn) {
                        if(synIndex < maxAmount - 1){
                            synonyms[synIndex] = objNode.asText();
                            synIndex++;
                        }
                    }
                }
                final JsonNode ant = item.path("antonyms");
                if (!ant.isEmpty() && ant.isArray()) {
                    for (final JsonNode objNode : ant) {
                        if(antIndex < maxAmount - 1){
                            antonyms[antIndex] = objNode.asText();
                            antIndex++;
                        }
                    }
                }
            }
        }

        modelAndView.addObject("word",word);
        modelAndView.addObject("synonyms",synonyms);
        modelAndView.addObject("antonyms",antonyms);
        modelAndView.setViewName("admin/fetch-words");
        return modelAndView;

    }

    @RequestMapping(value = "/admin/save-word", method = RequestMethod.POST)
    public RedirectView createNewWord(@Valid Word word,BindingResult bindingResult) {
        wordService.saveWord(word);
        return new RedirectView("/admin/words/1");
    }

    @RequestMapping(value = "/admin/delete-word", method = RequestMethod.POST)
    public RedirectView deleteExistingWord(@Valid Word word, BindingResult bindingResult) {
        wordService.deleteWord(word);
        return new RedirectView("/admin/words/1");
    }
}