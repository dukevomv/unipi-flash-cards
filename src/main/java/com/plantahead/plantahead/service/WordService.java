package com.plantahead.plantahead.service;

import com.plantahead.plantahead.model.Role;
import com.plantahead.plantahead.model.User;
import com.plantahead.plantahead.model.Word;
import com.plantahead.plantahead.repository.RoleRepository;
import com.plantahead.plantahead.repository.UserRepository;
import com.plantahead.plantahead.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class WordService {
    private WordRepository wordRepository;

    @Autowired
    public WordService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    public List<Word> getAll() {
        return wordRepository.findAll();
    }
    public List<Word> getSome() {
        return wordRepository.findTop40ByOrderByMainDesc();
    }
    public Word getByPair(String main, String secondary) {
        return wordRepository.findByMainAndSecondary(main,secondary);
    }

    public Word saveWord(Word word) {
        return wordRepository.save(word);
    }

    public void deleteWord(Word word) {
        Word current[] = { word };
        List<Word> ids = Arrays.asList(current);
        wordRepository.deleteInBatch(ids);
    }

    public Page < Word > findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ?
                Sort.by(sortField).ascending() : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return this.wordRepository.findAll(pageable);
    }
}
