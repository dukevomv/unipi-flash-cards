package com.plantahead.plantahead.service;

import com.plantahead.plantahead.model.Game;
import com.plantahead.plantahead.model.Word;
import com.plantahead.plantahead.repository.GameRepository;
import com.plantahead.plantahead.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService {
    private GameRepository gameRepository;

    @Autowired
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Game findById(int id) throws ChangeSetPersister.NotFoundException {
        return gameRepository.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);
    }
}
