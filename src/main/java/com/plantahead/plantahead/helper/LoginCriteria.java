package com.plantahead.plantahead.helper;

import org.hibernate.validator.constraints.NotBlank;

public class LoginCriteria {

    String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}