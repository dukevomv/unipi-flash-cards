package com.plantahead.plantahead.helper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VoteRequest {
    Integer game_id;
    String main;
    String secondary;
    Boolean is_synonym;
}
