package com.plantahead.plantahead.helper;

import com.plantahead.plantahead.model.Word;

import java.util.List;

public class VoteResponse {
    Boolean correct;
    Boolean canContinue = true;
    Boolean isHighscore = false;
    Integer life = 0;
    Integer points = 0;
    Integer finalScore = 0;

    public Boolean getCorrect() {
        return correct;
    }
    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Boolean getCanContinue() {
        return canContinue;
    }
    public void setCanContinue(Boolean canContinue) {
        this.canContinue = canContinue;
    }

    public Integer getLife() {
        return life;
    }
    public void setLife(Integer life) {
        this.life = life;
    }

    public Integer getPoints() {
        return points;
    }
    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getFinalScore() { return finalScore; }
    public void setFinalScore(Integer finalScore) { this.finalScore = finalScore; }

    public Boolean getIsHighscore() { return isHighscore; }
    public void setIsHighscore(Boolean isHighscore) { this.isHighscore = isHighscore; }
}
