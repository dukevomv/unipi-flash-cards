package com.plantahead.plantahead.helper;

import com.plantahead.plantahead.model.Word;

import java.util.List;

public class BasicAPIResponse {
    String msg;
    Boolean result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

}
