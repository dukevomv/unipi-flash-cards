package com.plantahead.plantahead.helper;

import com.plantahead.plantahead.model.Game;
import com.plantahead.plantahead.model.Word;

import java.util.List;

public class GameResponse {
    String msg;
    Integer id;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
