package com.plantahead.plantahead;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlantAheadApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlantAheadApplication.class, args);
	}

}
