package com.plantahead.plantahead.repository;

import com.plantahead.plantahead.model.Game;
import com.plantahead.plantahead.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    User findByUsername(String username);
    List<User> findTop10ByOrderByPointsDesc();

    interface IdAndRank{
        int getId();
        int getRank();
    }

    @Query(value = "SELECT id, FIND_IN_SET( points, (SELECT GROUP_CONCAT( points ORDER BY points DESC ) FROM users ) ) AS rank FROM users WHERE id = ?1",nativeQuery = true)
    IdAndRank getUserRank(@Param("user") int user_id);
}