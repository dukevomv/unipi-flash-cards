package com.plantahead.plantahead.repository;

import com.plantahead.plantahead.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer> {

    @Query(value = "select g from Game g")
    Game findOneByUserId(@Param("user") int user_id);

    Game findByUsername(String username);

}