package com.plantahead.plantahead.repository;

import com.plantahead.plantahead.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WordRepository extends JpaRepository<Word, Integer> {
    List<Word> findAll();
    List<Word> findTop40ByOrderByMainDesc();
    Word findByMainAndSecondary(String main, String secondary);
    void deleteInBatch(Iterable<Word> ids);
}