
$(document).ready(function() {

fetchCards();

});



async function voteWordAPI(main,secondary,vote){
    return new Promise(function (res,rej){
        const is_synonym = vote === 'synonym'
        const game_id = getGameId()
        $.ajax({
            url:"/api/vote",
            type:"post",
            data: JSON.stringify({main,secondary,is_synonym,game_id}),
            contentType:"application/json",
            dataType:"json",
            success:function(data){
                res(data);
            }
        });
    })
}

async function voteWord(main,secondary,vote){
    let response = await voteWordAPI(main,secondary,vote);
    return response;

//    const success = {
//        correct: true,
//        points: 150,
//        life: 1,
//        canContinue: true
//    }
//    let error = {
//        correct: false,
//        life: -1,
//        canContinue: false,
//        isHighscore: false
//    }
//    if(vote === 'synonym'){
//        return success
//    } else if(vote === 'antonym'){
//        return error
//    }
}

async function voteForCard(main,secondary,vote){
    voteResults = await voteWord(main,secondary,vote);
    if(voteResults.points){
        addPoints(voteResults.points)
    }
    if(voteResults.life){
        for(let i=0;i<Math.abs(voteResults.life);i++){
            if(voteResults.life > 0){
                addLife(true)
            } else {
                removeLife(true)
            }
        }
    }
    if(!voteResults.canContinue){
        updateGameOverModal(voteResults.finalScore,voteResults.isHighscore);
    }
}

function updateGameOverModal(score,isHighScore){
    $('#gameOverModal .points-text').text(score);
    $('#gameOverModal .high-score-points-text').text(score);
    if(isHighScore){
        $('#gameOverModal .high-score').removeClass('hidden');
        $('#gameOverModal .failed').addClass('hidden');
    } else {
        $('#gameOverModal .failed').removeClass('hidden');
        $('#gameOverModal .high-score').addClass('hidden');
    }
    $('#gameOverModal').modal('show');
}

let pendingCards = [];

function updateMainWord(){
    let card = $('.card_box .card.active').last()
    $('.main-word').text(card.data('main-word'))
}

const colors = ['purple','green'];
let colorIndex = 0;
function getCardColor(){
    colorIndex++;
    if(colorIndex == colors.length){
        colorIndex = 0;
    }
    return colors[colorIndex];
}
function appendCard(word){
    let cardDOM = `<div class="card ${getCardColor()}" data-main-word="${word.main}" data-secondary-word="${word.secondary}">
                    <div class="avatar word-font">
                     ${word.secondary}
                    </div>
                    <div class="choice accept">
                     <div class="avatar word-font">
                       ${word.secondary}
                       <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                     </div>
                    </div>
                    <div class="choice reject">
                     <div class="avatar word-font">
                       ${word.secondary}
                       <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                     </div>
                    </div>
                   </div>`
    $('.card_box').prepend(cardDOM);
}
async function fetchCards(){
    pendingCards = await getWords();
    do{
        appendCard(pendingCards.pop());
    } while(pendingCards.length > 0)
}

function toggleUserModals(action){
    switch(action){
        case 'login':
            $('#userRegisterModal').modal('hide');
            $('#userLoginModal').modal('show');
            break;
        case 'register':
            $('#userLoginModal').modal('hide');
            $('#userRegisterModal').modal('show');
            break;
    }
}

function addPoints(pointsToAdd){
    const idTag = 'user-score';
    let current = parseInt($('#'+idTag).text());
    goToPoints(current+pointsToAdd);
}

function resetPoints(){
    const idTag = 'user-score';
    let current = $('#'+idTag).text(0);
}

function goToPoints(finalPoints){
    const idTag = 'user-score';
    let current = parseInt($('#'+idTag).text());
    numberCounter(idTag, current, finalPoints, 1000);
//    updateScoreboard()
}

async function getTopPlayers(number){
    //deprecated
    return [
        {username:"magi",points:121212,rank:1},
        {username:"andre",points:1123,rank:2},
        {username:"duke",points:32323,rank:3},
        {username:"wizzy",points:121212,rank:4},
        {username:"aris",points:333,rank:5}
    ]
}
async function getUserRank(){
    //deprecated
    return {username:"duke",points:2222,rank:2}
}
function appendScoreRow(rank,username,points,you = false){
        let classes = 'values';
        let youDOM = ''
        if(you){
            classes += ' you'
            youDOM = '<span class="label label-default">YOU</span>'
        }
        scoreDOM = `<tr class="${classes}">
                        <td>${rank} ${youDOM}</td>
                        <td>${username}</td>
                        <td class="text-right">${points}</td>
                    </tr>`
        $('#scoreboardModal table').append(scoreDOM);
}
function appendExtraUserScoreRow(rank,username,points){
    appendScoreRow('...','...','...');
    appendScoreRow(rank,username,points,true);
}
async function updateScoreboard(){
    return 1;
    //deprecated
    const topCount = 5
    let top = await getTopPlayers(topCount);
    score = await getUserRank();
    $('#scoreboardModal tr.values').remove();
    let youShown = false;
    for(let i =0;i<top.length;i++){
        currentUser = top[i].username === score.username;
        if(currentUser){
            youShown = true;
        }
        appendScoreRow(top[i].rank,top[i].username,top[i].points,currentUser);
    }
    if(!youShown){
        appendExtraUserScoreRow(score.rank,score.username,score.points);
    }
}

function addLife(force = false){
    let images = $('.action-wrap.life').find('.image-wrap');
    let added = false;
    images.each(function(i){
        if(!added && $(this).hasClass('disabled')){
            $(this).removeClass('disabled');
            added = true;
        }
        if(force && !added && $(this).hasClass('hidden')){
            $(this).removeClass('hidden');
            $(this).removeClass('disabled');
            added = true;
        }
    })
}
function removeLife(){
    let images = $('.action-wrap.life').find('.image-wrap');
    let removed = false;
    let index = 0;
    $(images.get().reverse()).each(function(i){
        if(!removed && !$(this).hasClass('disabled')){
            $(this).addClass('disabled');
            removed = true;
        }
        index++;
    })
}

function numberCounter(id, start, end, duration) {
  let obj = document.getElementById(id),
   current = start,
   range = end - start,
   increment = end > start ? 1 : -1,
   step = Math.abs(Math.floor(duration / range)),
   timer = setInterval(() => {
    current += increment;
    obj.textContent = current;
    if (current == end) {
     clearInterval(timer);
    }
   }, step);
 }

async function getWords(){
    let words = await $.get('/api/words');
    return words.result;
}

function playAgain(){
    addLife();
    addLife();
    addLife();
    resetPoints();
    startGame();
    $('#gameOverModal').modal('hide');
}

let gameId = null;
async function startGame(){
    gameId = await startGameAPI();
}

function getGameId(){
    return gameId;
}

async function startGameAPI(){
    return new Promise(function (res,rej){
        $.ajax({
            url:"/api/start-game",
            type:"post",
            data: JSON.stringify({username:getUsername()}),
            contentType:"application/json",
            dataType:"json",
            success:function(data){
                res(data.id);
            }
        });
    })
}
function getUsername(){
    return $('#username').text()
}

window.game = {
    toggleUserModals,
    getWords,
    fetchCards,
    updateMainWord,
    voteForCard,
    startGame,
    updateScoreboard,
    playAgain
}