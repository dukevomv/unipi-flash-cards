create table if not exists users(
	id int not null AUTO_INCREMENT unique,
	username varchar(512) not null unique,
	email varchar(512) not null,
	password varchar(512) not null,
	points int default 0 not null,
    primary key (id)
);

create table if not exists roles(
	role_id int not null,
	role varchar(512) not null unique,
    primary key (role_id)
);

create table if not exists user_role(
	role_id int not null,
	user_id int not null,
);

create table if not exists words(
	id int not null AUTO_INCREMENT unique,
	main varchar(512) not null,
	secondary varchar(512) not null,
	is_synonym tinyint not null default 1
);

create table if not exists games(
	id int not null AUTO_INCREMENT unique,
	username varchar(512) not null,
	score int not null default 0,
	lives int not null default 3,
	started_at timestamp not null default CURRENT_TIMESTAMP,
	finished_at timestamp null default null
);

create unique index if not exists users_email_uindex
	on users (email);

create unique index if not exists users_id_uindex
	on users (id);

